@extends('welcome')
@section('content')
<form action="{{route('store.student')}}" method="post">
    @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" name="name" id="exampleInputEmail1">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control" name="email" id="exampleInputEmail1">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input type="text" class="form-control" name="phone" id="exampleInputEmail1">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection()