<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentsController extends Controller
{
    //
    public function student(){
        return view('student.addstudent');
    }
    public function saveStudent(Request $request){
        $student = new Student;
        $student->name= $request->name;
        $student->email= $request->email;
        $student->phone= $request->phone;
        // return response()->json($student);
        $student->save();
    }
}
